import React, {useState} from 'react';
import Auxilliary from "../Auxilliary/Auxilliary";
import classes from './Layout.css';
import Toolbar from "../../components/Navigation/Toolbar/Toolbar";
import SideDrawer from "../../components/Navigation/SideDrawer/SideDrawer";
import { connect } from 'react-redux'

const Layout = ( props ) => {
  const [state, setState] = useState({
    showSideDrawer: false
  });
  
  const sideDrawerOpenHandler = () => {
    const showSideDrawer = !state.showSideDrawer;
    setState({
      ...state,
      showSideDrawer: showSideDrawer
    })
  };
  
  const sideDrawerClosedHandler = () => {
    setState({
      ...state,
      showSideDrawer: false
    })
  };
  
  return (
    <Auxilliary>
      <Toolbar
        isAuth={props.isAuthenticated}
        openSideDrawer={sideDrawerOpenHandler}/>
      <SideDrawer
        isAuth={props.isAuthenticated}
        open={state.showSideDrawer}
        closed={sideDrawerClosedHandler}/>
      <div>Toolbar, SideDrawer, Backdrop</div>
      <main className={ classes.Content }>
        { props.children }
      </main>
    </Auxilliary>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null //this will make the prop a boolean
  };
};

export default connect(mapStateToProps)(Layout);