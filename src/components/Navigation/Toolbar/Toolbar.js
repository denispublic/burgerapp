import React from 'react';
import classes from './Toolbar.css'
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import SideDrawerToggler from "./SideDrawerToggler/SideDrawerToggler";

const Toolbar = (props) => {
  return (
    <header className={ classes.Toolbar }>
      <SideDrawerToggler openSideDrawer={props.openSideDrawer} />
      <div className={ classes.Logo }>
        <Logo />
      </div>
      <nav className={classes.DesktopOnly}>
        <NavigationItems isAuthenticated={props.isAuth}/>
      </nav>
    </header>
  );
};

export default Toolbar;