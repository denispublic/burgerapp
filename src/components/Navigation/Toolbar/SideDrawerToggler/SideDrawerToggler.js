import React from 'react';
import Auxilliary from "../../../../hoc/Auxilliary/Auxilliary";
import classes from "./SideDrawerToggler.css";

const SideDrawerToggler = (props) => (
  <Auxilliary>
    <div onClick={props.openSideDrawer} className={classes.DrawerToggle}>
      <div></div>
      <div></div>
      <div></div>
    </div>
  </Auxilliary>
);

export default SideDrawerToggler;