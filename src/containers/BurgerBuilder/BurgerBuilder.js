import React, { useState, useEffect } from 'react';
import Auxilliary from "../../hoc/Auxilliary/Auxilliary";
import Burger from "../../components/Burger/Burger"
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import {connect} from 'react-redux';
import * as actions from '../../store/actions/index'
import axios from "../../axios-orders";

const BurgerBuilder = (props) => {
  const [state, setState] = useState({
    purchasing: false,
  });
  
  useEffect(() =>{
    props.onInitIngredients();
  }, []);
  
  const updatePurchaseState = (ingredients) => {
    const sum = Object.keys(ingredients).map(igKey => {
      return ingredients[igKey]
    }).reduce((sum, el) => {
      return sum + el;
    }, 0);
      return sum > 0
  };


  const disabledInfo = {
    ...props.ings
  };
  for (let key in disabledInfo) {
    disabledInfo[key] = disabledInfo[key] <= 0; //will return true or false
  }
  
  const purchaseHandler = () => {
    if(props.isAuthenticated) {
      setState({
        ...state,
        purchasing: true
      })
    } else {
      props.onSetAuthRedirectPath('/checkout');
      props.history.push('/auth');
    }

  };
  
  const purchaseCancelHandler = () => {
    setState({
      ...state,
      purchasing: false
    })
  };
  const purchaseContinueHandler = () => {
    props.onInitPurchase();
    props.history.push('/checkout');
  };
  let orderSummary = null;
  let burger = props.error ? <p>Ingredients cant be loaded!</p> : <Spinner/>;
  
  if(props.ings) {
    orderSummary =
      <OrderSummary ingredients={ props.ings }
                    price={ props.price }
                    purchaseCancelled={ purchaseCancelHandler }
                    purchaseContinued={ purchaseContinueHandler }
      />;
    burger = (
      <Auxilliary>
        <Burger ingredients={ props.ings }/>
        <BuildControls
          ingredientAdded={ props.onIngredientAdded }
          ingredientRemoved={ props.onIngredientRemoved }
          disabled={ disabledInfo }
          purchasable={ updatePurchaseState(props.ings) }
          ordered={ purchaseHandler }
          isAuth={props.isAuthenticated}
          price={ props.price } />
      </Auxilliary> );
  }

  return (
    <Auxilliary>
      <Modal show={ state.purchasing }
             modalClosed={ purchaseCancelHandler }>
            { orderSummary }
      </Modal>
      { burger }
    </Auxilliary>
  );
};

const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    error: state.burgerBuilder.error,
    isAuthenticated: state.auth.token != null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onIngredientAdded: (igName) => dispatch(actions.addIngredient(igName)),
    onIngredientRemoved: (igName) => dispatch(actions.removeIngredient(igName)),
    onInitIngredients: () => dispatch(actions.initIngredients()),
    onInitPurchase: () => dispatch(actions.purchaseInit()),
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));