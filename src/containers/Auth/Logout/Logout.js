import React, {useEffect} from "react";
import * as actios from '../../../store/actions/index';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'

const Logout = (props) => {
  useEffect(() => {
    props.onLogout();
  }, []);

  return (
    <Redirect to="/" />
  );
};

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => dispatch(actios.logout())
  }
};

export default connect(null, mapDispatchToProps)(Logout);